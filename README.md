This folder/repository contains my application for ANR JCJC funding
during the academic year 2018-2019.

The LaTeX templates I used are available separately at
  https://gitlab.com/gasche/anr-2019-jcjc-template/